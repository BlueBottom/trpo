Rails.application.routes.draw do
  get 'admin', to: "admin#index", as: :admin_panel
  get '/tickets/invalidate/:id', to: "tickets#invalidate_ticket", as: :invalidate_ticket
  get '/tickets/filter', to: "tickets#filter"
  get '/tickets/add/:id', to: "tickets#add_ticket", as: :ticket_add
  get '/profile/remove/:id', to: "tickets#remove_ticket", as: :ticket_remove
  resources :tickets
  root "home#index"
  get '/profile', to: "home#profile", as: :profile
  get '/home/index', to: "home#index"
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions' }
end