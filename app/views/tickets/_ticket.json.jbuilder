json.extract! ticket, :id, :from, :to, :price, :start_date, :finish_date, :type, :user_id, :created_at, :updated_at
json.url ticket_url(ticket, format: :json)
