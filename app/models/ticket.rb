class Ticket < ApplicationRecord
  has_and_belongs_to_many :users

  validates :from, presence: true,  length: {minimum: 1}
  validates :to, presence: true,  length: {minimum: 1}
  validates :price, presence: true, comparison: {greater_than: 0}
  validates :start_date, presence: true,  comparison: { greater_than: Date.current }
  validates :finish_date, presence: true,  comparison: { greater_than: Date.current }
  validates :transport, presence: true, length: {minimum: 1}
end
