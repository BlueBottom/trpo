class TicketsController < ApplicationController
  before_action :authenticate_user!, only: %i[ add_ticket remove_ticket ]
  before_action :set_ticket, only: %i[ add_ticket show edit update destroy ]
  before_action :store_user_location!, if: :storable_location?

  # GET /tickets or /tickets.json
  def index
    @tickets = Ticket.where(relevant: true).sort
    filter_params
  end

  # GET /tickets/1 or /tickets/1.json
  def show
  end

  def add_ticket
    user = User.find(current_user.id)
    user.tickets << @ticket if user.tickets.where(id: @ticket.id).first.blank?
    redirect_to :ticket
  end

  def remove_ticket
    user = User.find(current_user.id)
    user.tickets.delete(params[:id])
    redirect_to :profile
  end

  def invalidate_ticket
    ticket = Ticket.find(params[:id])
    ticket.relevant = !ticket.relevant
    ticket.save
    redirect_to :admin_panel
  end

  def filter
  end

  # GET /tickets/new
  def new
    authenticate_user! unless user_signed_in?
    @ticket = Ticket.new
    authorize @ticket
  rescue Pundit::NotAuthorizedError
    redirect_to root_path
  end

  # GET /tickets/1/edit
  def edit
    authorize @ticket
  rescue Pundit::NotAuthorizedError
    redirect_to root_path
  end

  # POST /tickets or /tickets.json
  def create
    @ticket = Ticket.new(ticket_params)

    respond_to do |format|
      if @ticket.save
        format.html { redirect_to root_path, notice: "Ticket was successfully created." }
        format.json { render :show, status: :created, location: @ticket }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /tickets/1 or /tickets/1.json
  def update
    respond_to do |format|
      if @ticket.update(ticket_params)
        format.html { redirect_to root_path, notice: "Ticket was successfully updated." }
        format.json { render :show, status: :ok, location: @ticket }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tickets/1 or /tickets/1.json
  def destroy
    begin
      authorize @ticket
      rescue Pundit::NotAuthorizedError
        redirect_to root_path
        return
    end

    @ticket.destroy!

    respond_to do |format|
      format.html { redirect_to admin_panel_path, notice: "Ticket was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_ticket
    @ticket = Ticket.find(params[:id])
  end

  # Only allow a list of trusted parameters through.
  def ticket_params
    authenticate_user! unless user_signed_in?
    params.require(:ticket).permit(:from, :to, :price, :start_date, :finish_date, :transport, :relevant)
          .with_defaults(relevant: true)
  end

  def storable_location?
    request.get? &&
      is_navigational_format? &&
      !devise_controller? &&
      !request.xhr?
  end

  def store_user_location!
    # :user is the scope we are authenticating
    store_location_for(:user, request.fullpath)
  end

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope)
  end

  def filter_params
    @tickets = @tickets.where(from: params[:from]) unless params[:from].blank?
    unless params[:to].blank?
      @tickets = @tickets.where(to: params[:to])
    end
    unless params[:price].blank?
      @tickets = @tickets.where(price: params[:price])
    end
    unless params[:start_date].blank?
      @tickets = @tickets.where(start_date: params[:start_date])
    end
    unless params[:finish_date].blank?
      @tickets = @tickets.where(finish_date: params[:finish_date])
    end
    unless params[:transport].blank?
      @tickets = @tickets.where(transport: params[:transport])
    end
  end
end
