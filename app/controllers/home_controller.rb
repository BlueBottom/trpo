class HomeController < ApplicationController
  protect_from_forgery prepend: true
  before_action :authenticate_user!, only: %i[ profile ]

  def index
    @tickets = Ticket.all
    filter_params
  end

  def profile
    user = User.find(current_user.id)
    @tickets = user.tickets.distinct
  end

  private

  def filter_params
    unless params[:from].blank?
    @tickets = @tickets.where(from: params[:from])
    end
    unless params[:to].blank?
      @tickets = @tickets.where(to: params[:to])
    end
    unless params[:price].blank?
      @tickets = @tickets.where(price: params[:price])
    end
    unless params[:start_date].blank?
      @tickets = @tickets.where(start_date: params[:start_date])
    end
    unless params[:finish_date].blank?
      @tickets = @tickets.where(finish_date: params[:finish_date])
    end
    unless params[:transport].blank?
      @tickets = @tickets.where(transport: params[:transport])
    end
  end

end
