class CreateTickets < ActiveRecord::Migration[7.1]
  def change
    create_table :tickets do |t|
      t.string :from
      t.string :to
      t.decimal :price
      t.date :start_date
      t.date :finish_date
      t.string :transport
      t.boolean :relevant

      t.timestamps
    end
  end
end
